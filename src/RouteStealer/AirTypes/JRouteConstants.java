package RouteStealer.AirTypes;

/**
 * Created by kdma on 08/05/2015.
 */
public class JRouteConstants {
    public final static String DEPARTURE = "Departure";
    public final static String ARRIVAL = "Arrival";
    public final static String WEIGHT = "Weight";
}
