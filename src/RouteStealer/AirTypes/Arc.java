package RouteStealer.AirTypes;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by kdma on 12/04/2015.
 */
public class Arc implements Comparable{


    private Airport _dep;
    private Airport _arr;
    private int _weight;

        public Arc(){
            _dep = new Airport();
            _arr = new Airport();
        }

    public Arc(Airport departure, Airport arrival){
        this(departure,arrival,1);
    }

    public Arc(Airport departure, Airport arrival,int weight){
        _dep = departure;
        _arr = arrival;
        _weight = weight;
    }

    public void incrementWeight(){
        _weight++;
    }

    public String getWeight(){
        return String.valueOf(_weight);
    }

    public String getDeparturCode() {
        return _arr.get_airportCode();
    }

    public String getArriveCode() {
        return _dep.get_airportCode();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Arc))
            return false;
        if (obj == this)
            return true;

        return new EqualsBuilder().
                append(_dep.get_airportCode(), ((Arc) obj)._dep.get_airportCode()).
                append(_arr.get_airportCode(), ((Arc) obj)._arr.get_airportCode()).
                isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
                append(_arr.get_airportCode()).
                append(_dep.get_airportCode()).
                toHashCode();
    }


    @Override
    public int compareTo(Object o) {
        if (!(o instanceof Arc))
            return 0;
        else{
            //sort by weight in decreasing order
            return -(_weight - ((Arc) o)._weight);
        }
    }
}
