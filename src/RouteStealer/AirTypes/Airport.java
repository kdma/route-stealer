package RouteStealer.AirTypes;

/**
 * Created by kdma on 11/04/2015.
 */
public class Airport {

    private String _cityName;
    private String _airportCode;
    private String _latitude;
    private String _longitude;


    public Airport(){}

    public Airport(String airportCode){
        this("",airportCode);
    }

    public Airport(String cityName, String airportCode){
         _cityName = cityName;
        _airportCode = airportCode;
    }

    public Airport(String cityName, String airportCode, String latitude, String longitude){
        this(cityName, airportCode);
        _latitude = latitude;
        _longitude = longitude;

    }

    public String get_cityName() {
        return _cityName;
    }

    public String get_airportCode() {
        return _airportCode;
    }

}
