package RouteStealer.Fetchers;

import RouteStealer.AirTypes.Arc;

import java.util.HashSet;

/**
 * Created by kdma on 13/05/2015.
 */
public class FetchingResultSet {
    private HashSet<Arc> _arcs;
    private String _domain;

    public String GetDomain() {
        return _domain;
    }

    public HashSet<Arc> getArcs() {

        return _arcs;
    }

    public FetchingResultSet(HashSet<Arc> arcs, String dom){
        _arcs = arcs;
        _domain = dom;
    }
}
