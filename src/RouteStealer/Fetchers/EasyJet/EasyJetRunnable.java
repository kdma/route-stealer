package RouteStealer.Fetchers.EasyJet;

import RouteStealer.AirTypes.Arc;
import RouteStealer.Fetchers.FetchingResultSet;
import RouteStealer.Fetchers.RoutableThread;

import java.util.HashSet;

/**
 * Created by kdma on 03/05/2015.
 */
public class EasyJetRunnable implements RoutableThread {
    private EasyJetFetcher _easyJetFetcher;
    private HashSet<Arc> _result;
    private String _domain;
    private String _subStringDomain;

    public EasyJetRunnable(String domain){
        _domain = domain;
        _easyJetFetcher = new EasyJetFetcher(_domain);
        _result = new HashSet<>();
    }
    @Override
    public HashSet<Arc> getSetOfArcs() {
        return _result;
    }

    @Override
    public FetchingResultSet call() {
        _result = _easyJetFetcher.getRoutes();
        _subStringDomain = _domain.substring(11,18);
        FetchingResultSet FetchingResultSet = new FetchingResultSet(getSetOfArcs(), _subStringDomain);
        return FetchingResultSet;
    }

}
