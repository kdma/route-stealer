package RouteStealer.Fetchers.EasyJet;

import RouteStealer.AirTypes.Airport;
import RouteStealer.AirTypes.Arc;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kdma on 03/05/2015.
 */
public class EasyJetFetcher {
    private String _domain;
    private final static String ENDPOINT = "linkedAirportsJSON";
    private final static Logger LOGGER = Logger.getLogger(EasyJetFetcher.class.getName());
    private final String SPLIT_REGEX = "\\|";
    private final String MULTIPLE_AIRPORTS_FROM_SAME_CITY = "\\*[A-Z]+";
    private HashSet<Arc> _routes;

    public EasyJetFetcher(String domain){
        _domain = domain;
        _routes = new HashSet<>();
    }

    public HashSet<Arc> getRoutes() {
        HttpResponse<String> stringHttpResponse = null;

        try {
            LOGGER.log(Level.INFO, "Retrieving cities from {0}", _domain);
            stringHttpResponse = Unirest.post(_domain + "{endpoint}")
                    .routeParam("endpoint", ENDPOINT)
                    .asString();
        } catch (UnirestException e) {
            LOGGER.log(Level.SEVERE, "Retrieving cities from {0} failed", _domain);
            System.out.print(e.getMessage());
        }
        return translateTextToArcs(stringHttpResponse);
    }

    private HashSet<Arc> translateTextToArcs(HttpResponse<String> stringHttpResponse) {
        //easyjet response is javascript code  embedded as a string
       return translateStringResponseToRoutes(stringHttpResponse.getBody());
    }

    private HashSet<Arc> translateStringResponseToRoutes(String rawBody){
        String[] statements = rawBody.split(";");
        String jsArrayAsText = statements[0];
        String[] unparsedRoutes = jsArrayAsText.split(",");
        createRoutes(unparsedRoutes);
        return _routes;
    }

    private void createRoutes(String[] unparsedRoutes) {
        for(int i = 0; i < unparsedRoutes.length; i++){
            if(i == 0){
                unparsedRoutes[i] = fixFirstUnparsedRoute(unparsedRoutes[i]);
            }
            String departure = unparsedRoutes[i].split(SPLIT_REGEX)[0].substring(1); //removes javascript apostrophe
            String arrival = unparsedRoutes[i].split(SPLIT_REGEX)[1];
            addOnlyMeaningfulRoutes(departure, arrival);
        }
    }

    private void addOnlyMeaningfulRoutes(String departure, String arrival) {

        //example *MI means every airport from Milan but doesnt mean that there is a route for each airport,
        //its just to display available routes from milan to some destination when you search on easyjet website
        Pattern cityPattern = Pattern.compile(MULTIPLE_AIRPORTS_FROM_SAME_CITY);
        Matcher departureMatcher = cityPattern.matcher(departure);
        Matcher arrivalMatcher = cityPattern.matcher(arrival);

        if(!departureMatcher.find() && !arrivalMatcher.find()){

            _routes.add(new Arc(new Airport(departure), new Airport(arrival)));
        }

    }

    private String fixFirstUnparsedRoute(String firstUnparsedRoute){
        //first unparsed route contains javascript rubbish assignment -> var ac_la = ['LGW|ABZ|2011-06-03|2016-02-28'
        String unparsedRoute = firstUnparsedRoute.substring(13);
        return unparsedRoute;
    }
}
