package RouteStealer.Fetchers.FlightMaps;

import RouteStealer.AirTypes.Airport;
import RouteStealer.AirTypes.Arc;
import RouteStealer.helpers.WebDomains;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;
import org.jsoup.parser.Parser;

import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by kdma on 14/05/2015.
 */
public class DeltaAirlineFetcher extends AbstractFlightMapsFetcher {
    private final static Logger LOGGER = Logger.getLogger(DeltaAirlineFetcher.class.getName());
    public DeltaAirlineFetcher(String domain){
        super(domain);
    }
    @Override
    public HashSet<Arc> getRoutesFrom(Airport airport) {
        HttpResponse<String> response = null;

        try {
            response = Unirest.post(_domain + "{endpoint}")
                    .routeParam("endpoint", ROUTES_ENDPOINT)
                    .field(WebDomains.FlightMapsHTTPParameters.initial, "")
                    .field(WebDomains.FlightMapsHTTPParameters.from, airport.get_cityName())
                    .field(WebDomains.FlightMapsHTTPParameters.fromID, airport.get_airportCode())
                    .field(WebDomains.FlightMapsHTTPParameters.fromLocationType, 1)
                    .field(WebDomains.FlightMapsHTTPParameters.connections, true)
                    .field(WebDomains.FlightMapsHTTPParameters.nonStop, true)
                    .field(WebDomains.FlightMapsHTTPParameters.nonStop, false)
                    .field(WebDomains.FlightMapsHTTPParameters.codeShares, true)
                    .field(WebDomains.FlightMapsHTTPParameters.codeShares, false)
                    .asString();
            LOGGER.log(Level.INFO, "Retrieving routes from {0}", airport.get_cityName());
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Retrieving routes from {0} failed", airport.get_cityName());
            System.out.print(e.getMessage());
            return new HashSet<>();
        }
        return getFirstPage(airport, response);
    }

    @Override
    public Document getNextRoutePage(Airport airport, int pageNumber) {
        HttpResponse<String> response = null;
        try {
            response = Unirest.post(_domain + "{endpoint}")
                    .routeParam("endpoint", ROUTES_ENDPOINT)
                    .field(WebDomains.FlightMapsHTTPParameters.page, pageNumber)
                    .field(WebDomains.FlightMapsHTTPParameters.from, airport.get_cityName())
                    .field(WebDomains.FlightMapsHTTPParameters.fromID, airport.get_airportCode())
                    .field(WebDomains.FlightMapsHTTPParameters.fromLocationType, 1)
                    .field(WebDomains.FlightMapsHTTPParameters.carrier, "")
                    .field(WebDomains.FlightMapsHTTPParameters.carrierID, "")
                    .field(WebDomains.FlightMapsHTTPParameters.connections, false)
                    .field(WebDomains.FlightMapsHTTPParameters.nonStop, true)
                    .field(WebDomains.FlightMapsHTTPParameters.codeShares, true)
                    .field(WebDomains.FlightMapsHTTPParameters.showAsList, false)
                    .field(WebDomains.FlightMapsHTTPParameters.sortBy, "NumberStops")
                    .asString();
            LOGGER.log(Level.INFO, "Retrieving page number {0}", pageNumber);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Retrieving page number {0} failed", pageNumber);
            System.out.print(e.getMessage());
            return new Document("");
        }
        Document page = Jsoup.parse(response.getBody(), "", Parser.xmlParser());
        page.outputSettings().escapeMode(Entities.EscapeMode.extended);
        return page;
    }
}
