package RouteStealer.Fetchers.FlightMaps;

import RouteStealer.helpers.WebDomains;

/**
 * Created by kdma on 14/05/2015.
 */
public class FlightMapsFetcherFactory {
    public AbstractFlightMapsFetcher getDomainFetcher(String domain) {
        switch (domain){
            case WebDomains.AIR_CANADA:
                return new AirCanadaFetcher(domain);
            case WebDomains.DELTA_AIRLINE:
                return new DeltaAirlineFetcher(domain);
            case WebDomains.FLY_DUBAI:
                return new FlyDubaiFetcher(domain);
            case WebDomains.GERMAN_ALLIANCE:
                return new GermanAllianceFetcher(domain);
            case WebDomains.JAPAN_AIRLINES:
                return new JapanAirlinesFetcher(domain);
            case WebDomains.MIDDLE_EAST_ALLIANCE:
                return new MiddleEastFetcher(domain);
            case WebDomains.ONE_WORLD:
                return new OneWorldFetcher(domain);
            case WebDomains.QATAR_AIRWAY:
                return new QatarAirwayFetcher(domain);
            case WebDomains.STAR_ALLIANCE:
                return new StarAllianceFetcher(domain);
            case WebDomains.VIRGIN_ATLANTIC:
                return new VirginAtlanticFetcher(domain);
            default:
                return new StarAllianceFetcher(domain);
        }
    }
}
