package RouteStealer.Fetchers.FlightMaps;

import RouteStealer.AirTypes.Airport;
import RouteStealer.AirTypes.Arc;
import RouteStealer.Fetchers.FetchingResultSet;
import RouteStealer.Fetchers.RoutableThread;

import java.util.HashSet;

/**
 * Created by kdma on 14/04/2015.
 */
public class FlightMapsRunnable implements RoutableThread {
    private HashSet<Arc> _resultSet;
    private String _subStringDomain;
    private String _domain;
    private FlightMapsFetcherFactory _istantiator;
    private AbstractFlightMapsFetcher _concreteFetcher;

    public FlightMapsRunnable(String domain) {
        _domain = domain;
        _resultSet = new HashSet<>();
        _istantiator = new FlightMapsFetcherFactory();
        _concreteFetcher = _istantiator.getDomainFetcher(_domain);
    }

    @Override
    public FetchingResultSet call() {
        Airport[] airports = _concreteFetcher.getCities();
        for (Airport airport : airports) {
            HashSet<Arc> partialResult = _concreteFetcher.getRoutesFrom(airport);
            updateWeightOnCollision(partialResult);
        }
        _subStringDomain = _domain.substring(7, 9);
        FetchingResultSet FetchingResultSet = new FetchingResultSet(getSetOfArcs(), _subStringDomain);
        return FetchingResultSet;
    }

    private void updateWeightOnCollision(HashSet<Arc> list) {
        for(Arc element : list){
            if(_resultSet.contains(element)){
                for (Arc arc : _resultSet){
                    if(arc.equals(element)){
                        arc.incrementWeight();
                    }
                }
            }else{
                _resultSet.add(element);
            }

        }
    }

    public HashSet<Arc> getSetOfArcs() {
        return _resultSet;
    }

}
