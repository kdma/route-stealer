package RouteStealer.Fetchers.FlightMaps;

import RouteStealer.AirTypes.Airport;
import RouteStealer.AirTypes.Arc;
import RouteStealer.helpers.HTMLResponseParser;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;
import org.jsoup.parser.Parser;

import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;

/**
 * Created by kdma on 14/05/2015.
 */
public abstract class AbstractFlightMapsFetcher {
    private final static String CITIES_ENDPOINT = "/data";
    protected final static String ROUTES_ENDPOINT = "/destinations";
    private final static Logger LOGGER = Logger.getLogger(AbstractFlightMapsFetcher.class.getName());
    private final static int TEN_SECONDS = 10000;
    protected final String _domain;

    public AbstractFlightMapsFetcher(String domain){
       _domain = domain;
    }

    protected HashSet<Arc> getFirstPage(Airport airport, HttpResponse<String> response) {
        Document firstPage = Jsoup.parse(response.getBody(), "", Parser.xmlParser());
        firstPage.outputSettings().escapeMode(Entities.EscapeMode.extended);
        HTMLResponseParser parser = new HTMLResponseParser(firstPage, _domain);
        int numberOfPages = parser.getNumbersOfPages();
        if (numberOfPages >= 0) {
            Document[] pages = fetchAllPages(airport, firstPage, numberOfPages);
            return parser.getRoutes(pages);
        } else {
            return new HashSet<>();
        }
    }

    abstract HashSet<Arc> getRoutesFrom(Airport airport);
    abstract Document getNextRoutePage(Airport airport, int pageNumber);

    protected Document[] fetchAllPages(Airport airport, Document firstPage, int numberOfPagesToBeFetched){
        Document[] pages;
        if(numberOfPagesToBeFetched == 0){
            pages = new Document[1];
            pages[0] = firstPage;
        }else {
            pages = new Document[numberOfPagesToBeFetched];
            pages[0] = firstPage;
            for (int i = 1; i < numberOfPagesToBeFetched; i++) {
                int pageOffset = i + 1;
                pages[i] = this.getNextRoutePage(airport, pageOffset);
            }
        }
        return pages;
    }

    public Airport[] getCities() {
        HttpResponse<JsonNode> jsonResponse = null;
        int attempts = 0;
        while (attempts < 3) {
            try {
                LOGGER.log(Level.INFO, "Retrieving cities from {0}", _domain);
                jsonResponse = Unirest.post(_domain + "{endpoint}")
                        .routeParam("endpoint", CITIES_ENDPOINT)
                        .asJson();
                attempts = 3;
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Retrieving cities from {0} failed", _domain);
                attempts++;
                try {
                    sleep(TEN_SECONDS);
                } catch (InterruptedException ex) {
                    LOGGER.log(Level.SEVERE, "sleep failed {0}", ex.getMessage());
                }
                System.out.print(e.getMessage());
            }
        }
        return parseCities(jsonResponse);

    }

    protected Airport[] parseCities(HttpResponse<JsonNode> response) {
        JsonNode body = response.getBody();
        JSONArray cities = (JSONArray) body.getObject().get("Cities");
        LOGGER.log(Level.INFO, "Found {0} cities", cities.length());
        Airport[] airports = new Airport[cities.length()];
        for (int i = 0; i < cities.length(); i++) {
            JSONObject airportJSONobject = cities.getJSONObject(i);
            airports[i] = new Airport(airportJSONobject.getString("Name"), airportJSONobject.getString("Code"));
        }
        return airports;
    }

}



