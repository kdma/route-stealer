package RouteStealer.Fetchers.RyanAir;

import RouteStealer.AirTypes.Airport;
import RouteStealer.AirTypes.Arc;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by kdma on 14/04/2015.
 */
public class RyanairFetcher {
    private String _domain;
    private final static String ENDPOINT = "/api/2/forms/flight-booking-selector/";
    private final static Logger LOGGER = Logger.getLogger(RyanairFetcher.class.getName());

    public RyanairFetcher(String domain){_domain = domain;}

    public HashSet<Arc> getRoutes() {
        HttpResponse<JsonNode> jsonResponse = null;

        try {
            LOGGER.log(Level.INFO, "Retrieving cities from {0}", _domain);
            jsonResponse = Unirest.post(_domain + "{endpoint}")
                    .routeParam("endpoint", ENDPOINT)
                    .asJson();
        } catch (UnirestException e) {
            LOGGER.log(Level.SEVERE, "Retrieving cities from {0} failed", _domain);
            System.out.print(e.getMessage());
        }

        return translateJSONtoArcs(jsonResponse);
    }

    private HashSet<Arc> translateJSONtoArcs(HttpResponse<JsonNode> response){

        JSONObject routes = response.getBody().getObject().getJSONObject("routes");
        HashSet<Arc> set = new HashSet<>();
        Iterator<String> keys = routes.keys();
        while(keys.hasNext()) {
            String departCode = keys.next();
            String arrivalCode;
            JSONArray values = routes.getJSONArray(departCode);
            for (int k = 0; k < values.length(); k++) {
                arrivalCode = (String) values.get(k);
                Arc arc = new Arc(new Airport(departCode), new Airport(arrivalCode));
                set.add(arc);
            }

        }
        return set;
    }

}
