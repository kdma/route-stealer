package RouteStealer.Fetchers.RyanAir;

import RouteStealer.AirTypes.Arc;
import RouteStealer.Fetchers.FetchingResultSet;
import RouteStealer.Fetchers.RoutableThread;

import java.util.HashSet;

/**
 * Created by kdma on 14/04/2015.
 */
public class RyanairRunnable implements RoutableThread {
    private RyanairFetcher _ryan;
    private HashSet<Arc> _result;
    private String _domain;
    private String _subStringDomain;

    public RyanairRunnable(String domain){
        _domain = domain;
        _ryan = new RyanairFetcher(_domain);
        _result = new HashSet<>();
    }
    @Override
    public FetchingResultSet call() {
        _result = _ryan.getRoutes();
        _subStringDomain = _domain.substring(11, 18);
        FetchingResultSet FetchingResultSet = new FetchingResultSet(getSetOfArcs(), _subStringDomain);
        return FetchingResultSet;
    }

    public HashSet<Arc> getSetOfArcs() {
        return _result;
    }

}
