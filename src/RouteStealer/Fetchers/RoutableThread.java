package RouteStealer.Fetchers;


import RouteStealer.AirTypes.Arc;

import java.util.HashSet;
import java.util.concurrent.Callable;

/**
 * Created by kdma on 15/04/2015.
 */
public interface RoutableThread extends Callable{
    HashSet<Arc> getSetOfArcs();
}
