package RouteStealer.helpers;

import RouteStealer.AirTypes.Airport;
import RouteStealer.AirTypes.Arc;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kdma on 11/04/2015.
 */
public class HTMLResponseParser {

    private Document _firstPage;
    private String LENGTH_CSS_SELECTOR;

    private final static String NUMBER_OF_RESULTS_REGEX = "\\d+";

    private final static double RESULT_PER_PAGE = 100.0;
    private final static String LENGTH_CSS_PLACEHOLDER = "div.pnl-actions > button:nth-child(1) > span:nth-child(%s) > span";
    private final static String DEPARTURE_SELECTOR = "tr[id^=result-summary-] > td:nth-child(1)";
    private final static String ARRIVAL_SELECTOR ="tr[id^=result-summary-] > td:nth-child(2)";
    private final static String MAYBE_CONNECTION_SELECTOR = "tr[id^=result-summary-] > td.col-connect";
    private final static String PARAGRAPH_SELECTOR = "p";
    private final static String SMALL_SELECTOR = "small";
    private final static String NON_STOP = "<p> Non-stop</p>";
    private final static String ONE_STOP = "<p>1 stop</p>";
    private final static String CONNECTING_OVER = "<p>Connecting over</p>";
    private final static int NORMAL_CASE_CHILD = 3;
    private final static int SPECIAL_CASE_CHILD = 4;
    private HashSet<Arc> _totalResult;
    private HashSet<Arc> _partialResult;


    public HTMLResponseParser (Document document, String domain){
        _firstPage = document;
        setPlaceholderForLengthParsing(domain);
        _totalResult = new HashSet<>();
    }

    private void setPlaceholderForLengthParsing(String domain) {
        if (domain.equalsIgnoreCase(WebDomains.MIDDLE_EAST_ALLIANCE)) {
            LENGTH_CSS_SELECTOR = String.format(LENGTH_CSS_PLACEHOLDER, SPECIAL_CASE_CHILD);
        } else {
            LENGTH_CSS_SELECTOR = String.format(LENGTH_CSS_PLACEHOLDER, NORMAL_CASE_CHILD);
        }
    }

    public int getNumbersOfPages(){

        Pattern pattern = Pattern.compile(NUMBER_OF_RESULTS_REGEX);
        Element resultElement = _firstPage.select(LENGTH_CSS_SELECTOR).first();
        if(resultElement == null){
            //no results
            return 0;
        }
        Matcher matcher = pattern.matcher(resultElement.toString());
        int resultNumber = 0;
        while (matcher.find()){
            resultNumber = Integer.parseInt(matcher.group());
        }

        int pagesToFetch = (int)Math.ceil(resultNumber/RESULT_PER_PAGE);
        return pagesToFetch;

    }

    public HashSet<Arc> getRoutes(Document[] pages) {
        _partialResult = new HashSet<>();

        for (Document page : pages) {
            Elements departurCol = page.select(DEPARTURE_SELECTOR);
            Elements arrivalCol = page.select(ARRIVAL_SELECTOR);
            Elements maybeConnectionCol = page.select(MAYBE_CONNECTION_SELECTOR);
            ParseRoutes(departurCol, arrivalCol, maybeConnectionCol);
        }
        return updateListOfArcs(_partialResult);
    }

    private void ParseRoutes(Elements departurCol, Elements arrivalCol, Elements maybeConnectionCol) {
        for (int i = 0; i < departurCol.size(); i++){
            String departureRow = departurCol.get(i).toString();
            String arrivalRow = arrivalCol.get(i).toString();
            Elements connectionParagraph = maybeConnectionCol.get(i).select(PARAGRAPH_SELECTOR);
            Elements cityDep = departurCol.get(i).select(SMALL_SELECTOR);
            Elements cityArr = arrivalCol.get(i).select(SMALL_SELECTOR);
            if(connectionParagraph.first().toString().equalsIgnoreCase(NON_STOP)){
                parseRoute(departureRow, arrivalRow, cityDep, cityArr);
            }else if(connectionParagraph.first().toString().equalsIgnoreCase(ONE_STOP)
                    || connectionParagraph.first().toString().equalsIgnoreCase(CONNECTING_OVER)){
                String stopRow = maybeConnectionCol.get(i).toString();
                Elements cityStop = maybeConnectionCol.get(i).select(SMALL_SELECTOR);
                parseRoute(departureRow, stopRow, cityDep, cityStop);
                parseRoute(stopRow, arrivalRow, cityStop, cityArr);
            }
        }
    }

    private void parseRoute(String depUnparsed, String arrUnparsed, Elements cityDep, Elements cityArr) {
        Arc simpleRoute = new Arc(new Airport(cityDep.first().text(), getIataInsideParenthesis(depUnparsed)),
                                  new Airport(cityArr.first().text(), getIataInsideParenthesis(arrUnparsed)));
        _partialResult.add(simpleRoute);
    }

    private String getIataInsideParenthesis(String str) {
        String iata = str.substring(str.indexOf('(') + 1, str.indexOf(')'));
        if (iata.equalsIgnoreCase("Itami")) {
            return "ITM";
        } else if (iata.equalsIgnoreCase("Haneda")) {
            return "HND";
        } else {
            return iata;
        }
    }

    private HashSet<Arc> updateListOfArcs(HashSet<Arc> partial){
        for(Arc arc : partial){
            updateWeightOnCollision(_totalResult, arc);
        }
        return _totalResult;
    }

    private void updateWeightOnCollision(HashSet<Arc> list, Arc arc) {
        if (list.contains(arc)) {
            for (Arc element : list) {
                if (element.equals(arc)) {
                    element.incrementWeight();
                }
            }
        }else{
            list.add(arc);
        }
    }
}