package RouteStealer.helpers;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kdma on 11/04/2015.
 */
 public class WebDomains {

    public final static String DELTA_AIRLINE = "http://dl.fltmaps.com/en";
    public final static String AIR_CANADA = "http://ac.fltmaps.com/en";
    public final static String STAR_ALLIANCE = "http://starmap.fltmaps.com/en";
    public final static String VIRGIN_ATLANTIC = "http://vs.fltmaps.com/en";
    public final static String GERMAN_ALLIANCE = "http://os.fltmaps.com/en";
    public final static String MIDDLE_EAST_ALLIANCE = "http://me.fltmaps.com/en";
    public final static String FLY_DUBAI = "http://fz.fltmaps.com/en";
    public final static String QATAR_AIRWAY = "http://qr.fltmaps.com/en";
    public final static String JAPAN_AIRLINES = "http://jl.fltmaps.com/en";
    public final static String ONE_WORLD = "http://onw.fltmaps.com/en";
    public final static String RYANAIR = "http://www.ryanair.com/it";
    public final static String EASYJET = "http://www.easyjet.com/US/";


    private Map<Integer,String> _map;

    public  WebDomains(){
        _map = new HashMap<Integer,String>();
        setMap();
    }

    public Map<Integer,String> getMap(){
        return _map;
    }

    private void setMap(){
        _map.put(0,DELTA_AIRLINE);
        _map.put(1,AIR_CANADA);
        _map.put(2,STAR_ALLIANCE);
        _map.put(3,VIRGIN_ATLANTIC);
        _map.put(4,GERMAN_ALLIANCE);
        _map.put(5,MIDDLE_EAST_ALLIANCE);
        _map.put(6,FLY_DUBAI);
        _map.put(7,QATAR_AIRWAY);
        _map.put(8,JAPAN_AIRLINES);
        _map.put(9,ONE_WORLD);
        _map.put(10, RYANAIR);
        _map.put(11, EASYJET);
    }

    public static class FlightMapsHTTPParameters {
        public final static String initial = "initialSearch";
        public final static String from = "Destinations.From";
        public final static String fromID = "Destinations.FromId";
        public final static String fromLocationType = "Destinations.FromLocationType";
        public final static String connections = "Destinations.Connections";
        public final static String nonStop = "Destinations.NonStop";
        public final static String codeShares = "Destinations.CodeShares";
        public final static String carrier = "Destinations.Carrier";
        public final static String carrierID = "Destinations.CarrierId";
        public final static String showAsList = "Destinations.ShowAsList";
        public final static String sortBy = "Destinations.SortBy";
        public final static String page = "Destinations.Page";
    }


}
