package RouteStealer;

import RouteStealer.AirTypes.Arc;
import RouteStealer.Fetchers.EasyJet.EasyJetRunnable;
import RouteStealer.Fetchers.FetchingResultSet;
import RouteStealer.Fetchers.FlightMaps.FlightMapsRunnable;
import RouteStealer.Fetchers.RoutableThread;
import RouteStealer.Fetchers.RyanAir.RyanairRunnable;
import RouteStealer.helpers.WebDomains;

import java.util.HashSet;
import java.util.concurrent.*;

/**
 * Created by kdma on 05/05/2015.
 */
public class WorkerManager {
    private final int WORKERS_NUMBER = 12;
    private ExecutorService _threadpool;
    private WebDomains _wb;
    private RoutableThread[] _workers;
    private HashSet<Arc> _total;
    private Writer _writer;
    private CompletionService<FetchingResultSet> _pool;

    public WorkerManager(){
        _writer = new Writer();
        _threadpool = Executors.newFixedThreadPool(WORKERS_NUMBER);
        _wb = new WebDomains();
        _total = new HashSet<Arc>();
        _workers = new RoutableThread[WORKERS_NUMBER];
        _pool = new ExecutorCompletionService<>(_threadpool);
    }

    public void RunWorkers() {
        for (int i = 0;i<WORKERS_NUMBER;i++){
            _workers[i] = new FlightMapsRunnable(_wb.getMap().get(i));
            if(i == 10){
                _workers[i] = new RyanairRunnable(_wb.getMap().get(i));
            }
            if(i == 11){
                _workers[i] = new EasyJetRunnable(_wb.getMap().get(i));
            }
            _pool.submit(_workers[i]);
        }
        _threadpool.shutdown();
        PollResults();
        writeGlobalSet();
    }

    private void PollResults(){
        for (int i = 0;i<WORKERS_NUMBER;i++) {
            try {
                final Future<FetchingResultSet> future = _pool.take();
                final FetchingResultSet content = future.get();
                _total.addAll(content.getArcs());
                _writer.writeToFile(content.getArcs(), content.GetDomain());
            } catch (Exception e) {
                System.out.print(e.getCause());
            }
        }
    }

    private void writeGlobalSet(){
        Writer writer = new Writer();
        writer.writeToFile(_total,"global");
    }


}
