package RouteStealer;

import RouteStealer.AirTypes.Arc;
import RouteStealer.AirTypes.JRouteConstants;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;

/**
 * Created by kdma on 13/04/2015.
 */
public class Writer {
    private final String W_ROUTES_PATH = "c:\\routes_output\\";

    public Writer(){
        ensureDirExists();
    }

    private void ensureDirExists(){
        File dir = new File(W_ROUTES_PATH);
        if (!dir.exists()) {
            boolean result = false;
            try{
                dir.mkdir();
            }
            catch(SecurityException se) {
                //handle
            }
        }
    }

    public void writeToFile(HashSet<Arc> set, String fileName){
        JSONArray jsonSet = new JSONArray();
        for (Arc arc : set){
            JSONObject Jarc = new JSONObject();
            Jarc.put(JRouteConstants.DEPARTURE, arc.getDeparturCode());
            Jarc.put(JRouteConstants.ARRIVAL, arc.getArriveCode());
            Jarc.put(JRouteConstants.WEIGHT, arc.getWeight());
            jsonSet.put(Jarc);
        }
        writeJson(jsonSet,fileName);
    }

    public void writeJson(JSONArray set, String fileName){
        try {
            String path = W_ROUTES_PATH+fileName+".json";
            FileWriter file = new FileWriter(path);
            file.write(set.toString());
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
