package RouteStealer;


/**
 * Created by kdma on 14/04/2015.
 */
public class mainMT {
     private WorkerManager manager;

    public mainMT(){
        manager = new WorkerManager();
    }

    public void start(){
        manager.RunWorkers();
    }


    public static void main(String args[]) {
        mainMT main = new mainMT();
        main.start();
        System.out.println("\nFinished all threads");
    }
}
