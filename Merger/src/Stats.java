import RouteStealer.AirTypes.JRouteConstants;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.HashSet;

/**
 * Created by kdma on 19/04/2015.
 */
public class Stats {

    private JSONArray _routes;

    public Stats(Merger merger){
        Merger _merger = merger;
        _routes = _merger.getAllRoutes();
    }

    public int getNumberOfUniqueAirports(){
        HashSet<String> airports = new HashSet<String>();
        for (int i=0;i<_routes.length();i++){
            JSONObject singleRoute = (JSONObject)_routes.get(i);
            airports.add((String)singleRoute.get(JRouteConstants.DEPARTURE));
            airports.add((String)singleRoute.get(JRouteConstants.ARRIVAL));
        }
        return airports.size();
    }

    public int getNumberOfRoutes(){
        return _routes.length();
    }



    public Double getGraphDensity(){
        double density;
        double numerator = (double)getNumberOfRoutes();
        double denominator = getNumberOfUniqueAirports();
        denominator = denominator *(getNumberOfUniqueAirports()-1);
        density = numerator/denominator;
        return density;
    }
}
