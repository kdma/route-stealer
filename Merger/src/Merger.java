import RouteStealer.AirTypes.Airport;
import RouteStealer.AirTypes.Arc;
import RouteStealer.AirTypes.JRouteConstants;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.net.URI;
import java.util.*;

/**
 * Created by kdma on 18/04/2015.
 */

public class Merger {
    private final String RO_FLT_MAPS_PATH = "c:\\routes_output\\global.json";
    private final String RO_OPEN_ROUTE_PATH = "c:\\routes_output\\openflight.json";
    private final String RO_EASYJET_PATH = "c:\\routes_output\\easyjet.json";
    private final String RO_RYANAIR_PATH = "c:\\routes_output\\ryanair.json";

    private HashSet<Arc> _routesArcs;
    private HashSet<Arc> _lowCostRoutes;
    private JSONArray _allRoutes;
    private JSONArray _easyJet;
    private JSONArray _ryanAir;
    private JSONArray _openflight;

    public Merger(){
        _routesArcs = new LinkedHashSet<Arc>();
        _lowCostRoutes = new LinkedHashSet<Arc>();
        _openflight = new JSONArray();
        _allRoutes = new JSONArray();
        _easyJet = new JSONArray();
        _ryanAir = new JSONArray();
    }

    public void MergeAll(){
        addOpenRoutes();
        System.out.println("Adding open routes");
        addFltRoutes();
        System.out.println("Adding flt routes");
        addEasyJetRoutes();
        System.out.println("Adding easyjet routes");
        addRyanAirRoutes();
        System.out.println("Adding ryanair routes");
    }

    public void writeListSortedByWeight() {
        List<Arc> hashsetAsList = new ArrayList(_routesArcs);
        JSONArray jsonSet = new JSONArray();
        System.out.println("sorting list");
        Collections.sort(hashsetAsList);
        for (Arc arc : hashsetAsList){
            JSONObject Jarc = new JSONObject();
            Jarc.put(JRouteConstants.DEPARTURE, arc.getDeparturCode());
            Jarc.put(JRouteConstants.ARRIVAL, arc.getArriveCode());
            Jarc.put(JRouteConstants.WEIGHT, arc.getWeight());
            jsonSet.put(Jarc);
        }
        try {
            String path = "c:\\routes_output\\"+"sorted.json";
            FileWriter file = new FileWriter(path);
            file.write(jsonSet.toString());
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void MergeLowCost(){
        addEasyJetRoutes();
        addRyanAirRoutes();
    }

    public JSONArray getAllRoutes(){
        _allRoutes = generateJSONfromArcs(_routesArcs);
        return _allRoutes;
    }

    public JSONArray getLowCostRoutes(){
        return mergeLowCostRoutes();
    }

    private void addFltRoutes(){
        try {
            JSONArray fltmaps =  loadRoutesFromJson(RO_FLT_MAPS_PATH);
            addRoutesAssertingUniqueness(fltmaps, _routesArcs);
        }catch (IOException e){
            System.out.print(e.getMessage());
        }
    }

    private void addRyanAirRoutes(){
        try {
            _ryanAir = loadRoutesFromJson(RO_RYANAIR_PATH);
            addRoutesAssertingUniqueness(_ryanAir, _routesArcs);
        }catch (IOException e){
            System.out.print(e.getMessage());
        }
    }

    private void addEasyJetRoutes() {
        try {
            _easyJet =  loadRoutesFromJson(RO_EASYJET_PATH);
            addRoutesAssertingUniqueness(_easyJet, _routesArcs);
        } catch (IOException e) {
            System.out.print(e.getMessage());
        }
    }

    private void addOpenRoutes(){
        try {
            _openflight =  loadRoutesFromJson(RO_OPEN_ROUTE_PATH);
            addRoutesAssertingUniqueness(_openflight, _routesArcs);
        } catch (IOException e) {
            System.out.print(e.getMessage());
        }
    }

    private JSONArray loadRoutesFromJson(String path) throws IOException{
        File file = new File(path);
        URI uri = file.toURI();
        JSONTokener tokener = new JSONTokener(uri.toURL().openStream());
        JSONArray routes =  new JSONArray(tokener);
        return routes;
    }

    private void updateWeightOnCollision(HashSet<Arc> list, Arc edge) {
        if (list.contains(edge)) {
            for (Arc arc : list) {
                if (arc.equals(edge)) {
                    arc.incrementWeight();
                }
            }
        }else{
            list.add(edge);
        }
    }

    private void addRoutesAssertingUniqueness(JSONArray container, HashSet<Arc> output) {
        for (int i = 0; i < container.length(); i++) {
            JSONObject route = (JSONObject) container.get(i);
            String departure = (String) route.get(JRouteConstants.DEPARTURE);
            String arrival = (String) route.get(JRouteConstants.ARRIVAL);
            String weightStr = (String)route.get(JRouteConstants.WEIGHT);
            int weight = Integer.valueOf(weightStr);
            Arc arc = new Arc(new Airport(departure), new Airport(arrival),weight);
            updateWeightOnCollision(output, arc);
        }
    }

    private JSONArray generateJSONfromArcs(HashSet<Arc> listOfArcs){
        JSONArray output = new JSONArray();
        for (Arc arc : listOfArcs) {
            JSONObject Jarc = new JSONObject();
            Jarc.put(JRouteConstants.DEPARTURE, arc.getDeparturCode());
            Jarc.put(JRouteConstants.ARRIVAL, arc.getArriveCode());
            Jarc.put(JRouteConstants.WEIGHT, arc.getWeight());
            output.put(Jarc);
        }
        return output;
    }

    private JSONArray mergeLowCostRoutes() {
        JSONArray merged;
        addRoutesAssertingUniqueness(_easyJet, _lowCostRoutes);
        addRoutesAssertingUniqueness(_ryanAir, _lowCostRoutes);
        merged = generateJSONfromArcs(_lowCostRoutes);
        return merged;
    }
}
