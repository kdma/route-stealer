import RouteStealer.AirTypes.JRouteConstants;
import RouteStealer.Writer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kdma on 19/05/2015.
 */
public class RoutesWithInfoWriter {
    //TODO REFACTOR THIS UGLY CLASS
    private final String IATA_LIST_PATH = "data\\airports.dat";
    private String ALL_ROUTES_PATH = "c:\\regions_output\\global.json";
    private final int IATA_COL = 4;
    private int CITY_COL = 2;
    private int LAT_COL = 6;
    private int LON_COL = 7;
    private JSONArray _inputRoutes;
    private JSONArray _output;
    private Writer _writer;
    private Map<String, LocationInfo> _iataLocationMap;

    public RoutesWithInfoWriter() {
        _inputRoutes = new JSONArray();
        _output = new JSONArray();
        _writer = new Writer();
        _iataLocationMap = new HashMap<>();
    }

    public void WriteRouteWithAdditionalInfo(){
        LoadRoutesAsJson();
        createIataToLocationMap();
        PopulationLoader lp = new PopulationLoader();
        _iataLocationMap = lp.GetMapWithPopulation();
        createRoutesWithInfo();
        _writer.writeJson(_output, "routeswithinfo");
    }

    private void LoadRoutesAsJson() {
        File file = new File(ALL_ROUTES_PATH);
        URI uri = file.toURI();
        try {
            JSONTokener tokener = new JSONTokener(uri.toURL().openStream());
            JSONArray routes = new JSONArray(tokener);
            _inputRoutes = routes;

        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }

    private void createIataToLocationMap() {
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(IATA_LIST_PATH));
            while ((line = br.readLine()) != null) {
                String[] row = line.split(cvsSplitBy);
                String iataCode = row[IATA_COL].replace("\"", "");
                String city = row[CITY_COL].replace("\"", "");
                String lat = row[LAT_COL];
                String lon = row[LON_COL];
                _iataLocationMap.put(iataCode, new LocationInfo(city, lat, lon));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void createRoutesWithInfo() {
        for(int i =0;i< _inputRoutes.length();i++) {
            JSONObject routeWithoutCoords = _inputRoutes.getJSONObject(i);
            JSONObject routeWithCoords = addRouteWithInfo(
                    (String) routeWithoutCoords.get(JRouteConstants.DEPARTURE),
                    (String) routeWithoutCoords.get(JRouteConstants.ARRIVAL),
                    (String) routeWithoutCoords.get(JRouteConstants.WEIGHT));

            if(routeWithCoords.length() != 0){
                _output.put(routeWithCoords);
            }
        }
    }

    private JSONObject addRouteWithInfo(String iataDep, String iataArr, String weight){
        JSONObject route = new JSONObject();
        if(_iataLocationMap.containsKey(iataDep) && _iataLocationMap.containsKey(iataArr)){
            JSONObject dep = buildRouteWithInfo(iataDep);
            JSONObject arr = buildRouteWithInfo(iataArr);
            route.put(JRouteConstants.WEIGHT, weight);
            route.put(JRouteConstants.DEPARTURE, dep);
            route.put(JRouteConstants.ARRIVAL, arr);
        }
        return route;
    }

    private JSONObject buildRouteWithInfo(String iata) {
        JSONObject obj = new JSONObject();
        obj.put(LocationInfo.IATA, iata);
        obj.put(LocationInfo.CITY, _iataLocationMap.get(iata)._city);
        obj.put(LocationInfo.LAT, _iataLocationMap.get(iata)._lat);
        obj.put(LocationInfo.LON, _iataLocationMap.get(iata)._lon);
        obj.put(LocationInfo.POP, _iataLocationMap.get(iata)._population);
        return obj;
    }

    public class LocationInfo {
        private String _city;
        private String _lat;
        private String _lon;
        private String _population;

        public static final String CITY = "city";
        public static final String IATA = "iata";
        public static final String LAT = "lat";
        public static final String LON = "lon";
        public static final String POP = "pop";


        public LocationInfo(String city, String lat, String lon){
            this(city, lat, lon, "");
        }

        public LocationInfo(String city, String lat, String lon, String population){
            _city = city;
            _lat = lat;
            _lon = lon;
            _population = population;
        }

    }

    public class PopulationLoader {
        private int CITY_COL = 0;
        private int ALT_CITY_COL = 1;
        private int POP_COL = 4;
        private final String POPULATION_PATH = "data\\population.csv";
        private final String JSON_PATH = "data\\population.json";
        private HashMap<String, LocationInfo> _fullmap;

        public PopulationLoader(){
            _fullmap = new HashMap<>();
        }

        private void UpdateLocationWithPopulation(String city, String city_alt, String pop){
            for (Map.Entry<String, LocationInfo> entry : _iataLocationMap.entrySet()) {
                String iata = entry.getKey();
                LocationInfo value = entry.getValue();
                if(value._city.equalsIgnoreCase(city)){
                    String lat = _iataLocationMap.get(iata)._lat;
                    String lon = _iataLocationMap.get(iata)._lon;
                    _fullmap.put(iata, new LocationInfo(city,lat,lon,pop));
                }else if(value._city.equalsIgnoreCase(city_alt)) {
                    String lat = _iataLocationMap.get(iata)._lat;
                    String lon = _iataLocationMap.get(iata)._lon;
                    _fullmap.put(iata, new LocationInfo(city_alt,lat,lon,pop));
                }
            }
        }

        private void LoadPopulationFromFirstFile() {
            BufferedReader br = null;
            String line;
            String cvsSplitBy = ";";
            try {
                br = new BufferedReader(new FileReader(POPULATION_PATH));
                while ((line = br.readLine()) != null) {
                    String[] row = line.split(cvsSplitBy);
                    String city = row[CITY_COL];
                    String city_alt = row[ALT_CITY_COL];
                    try{
                        Number population = NumberFormat.getNumberInstance(java.util.Locale.US).parse(row[POP_COL]);
                        population = population.intValue()*1000;
                        UpdateLocationWithPopulation(city, city_alt, population.toString());
                    }catch (Exception e){
                        System.out.print(e.getMessage());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        private void LoadPopulationFromSecondFile() {
            File file = new File(JSON_PATH);
            URI uri = file.toURI();
            JSONObject routes;
            try{
                JSONTokener tokener = new JSONTokener(uri.toURL().openStream());
                routes =  new JSONObject(tokener);
                JSONArray array = routes.getJSONArray("objs");
                for(int i = 0;i<array.length();i++){
                    JSONObject record = array.getJSONObject(i);
                    String city = record.getString("name");
                    String alt_city = record.getString("wiki");
                    String pop = String.valueOf(record.get("pop"));
                    UpdateLocationWithPopulation(city, alt_city, pop);
                }
            } catch (Exception e){
                System.out.print(e.getMessage());
            }
        }

        public HashMap<String, LocationInfo> GetMapWithPopulation(){
            LoadPopulationFromFirstFile();
            LoadPopulationFromSecondFile();
            return _fullmap;
        }
    }
}
