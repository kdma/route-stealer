package Dividers;

import RouteStealer.AirTypes.JRouteConstants;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by kdma on 05/05/2015.
 */
public abstract class RegionDivider {
    protected GeoRegion[] _regions;
    protected RegionMap _map;
    protected final int REGIONS_NUMBER = 9;
    protected JSONArray _routes;
    private final static Logger LOGGER = Logger.getLogger(LowCostDivider.class.getName());


    public RegionDivider(JSONArray routes){
        _routes = routes;
        _regions = new GeoRegion[REGIONS_NUMBER];
        _map = new RegionMap();
        initRegions();
    }

    public void DivideByRegion(){
        for(int i=0;i<_routes.length(); i++){
            JSONObject route = (JSONObject) _routes.get(i);
            String departure = route.getString(JRouteConstants.DEPARTURE);
            String arrival = route.getString(JRouteConstants.ARRIVAL);
            String depRegion = _map.getRegionFromIata(departure);
            String arrRegion = _map.getRegionFromIata(arrival);
            moveRouteIntoCorrectRegion(route, depRegion, arrRegion);
            LOGGER.log(Level.INFO, "get region for {0}", departure);
        }
        WriteByRegion();
    }

    protected abstract void WriteByRegion();

    private void initRegions() {
        for(int i = 0;i< REGIONS_NUMBER;i++){
            _regions[i] = new GeoRegion(_map.get(i));
        }
    }

    protected JSONArray getEuropeRoutes(){
        return _regions[0]._routes;
    }

    protected void moveRouteIntoCorrectRegion(JSONObject route, String depRegion, String arrRegion) {
        if(depRegion.equalsIgnoreCase(arrRegion)){
            String region = depRegion;
            int regionIndex = _map.get(region);
            _regions[regionIndex].put(route);
        }
    }

    public static class GeoRegion {
        private String _regionDenomination;
        private JSONArray _routes;

        public GeoRegion(String regionDenomination){
            _routes = new JSONArray();
            _regionDenomination = regionDenomination;
        }

        public void put(JSONObject obj){
            _routes.put(obj);
        }

        public JSONArray GetRoutes(){
            return _routes;
        }

        public String getRegionDenomination(){
            return _regionDenomination;
        }
    }

    public static class RegionMap {
        private final static Logger LOGGER = Logger.getLogger(RegionMap.class.getName());

        private HashMap<String,Integer> _forwardRegionMap;
        private HashMap<Integer, String> _inverseRegionMap;
        private HashMap<String,String> _iataToRegion;
        private final String IATA_LIST_PATH = "data\\airports.dat";
        private final int IATA_COL = 4;
        private final int REGION_COL = 11;


        public RegionMap(){
            _forwardRegionMap = new HashMap<String,Integer>();
            _inverseRegionMap = new HashMap<Integer,String>();
            _iataToRegion = new HashMap<String,String>();
            populateMaps();
        }

        public String getRegionFromIata(String iata) {
            if (_iataToRegion.containsKey(iata)) {
                return _iataToRegion.get(iata);
            } else {
                return _inverseRegionMap.get(8); //string other
            }
        }

        public int get(String key){
            LOGGER.log(Level.INFO, "get key {0} from regionmap", key);
            if(_forwardRegionMap.containsKey(key)) {
                return _forwardRegionMap.get(key);
            }else{
                return _forwardRegionMap.get("Other");//index of other
            }
        }

        public String get(int key){
            return _inverseRegionMap.get(key);
        }

        private void populateMaps() {
            loadForwardMap();
            loadInverseMap();
            loadIATAtoAirportMap();
         }

        private void loadInverseMap() {
            _inverseRegionMap.put(0, "Europe");
            _inverseRegionMap.put(1, "America");
            _inverseRegionMap.put(2, "Australia");
            _inverseRegionMap.put(3, "Africa");
            _inverseRegionMap.put(4, "Asia");
            _inverseRegionMap.put(5, "Pacific");
            _inverseRegionMap.put(6, "Atlantic");
            _inverseRegionMap.put(7, "Indian");
            _inverseRegionMap.put(8, "Other");

        }

        private void loadForwardMap() {
            _forwardRegionMap.put("Europe", 0);
            _forwardRegionMap.put("America", 1);
            _forwardRegionMap.put("Australia", 2);
            _forwardRegionMap.put("Africa", 3);
            _forwardRegionMap.put("Asia", 4);
            _forwardRegionMap.put("Pacific", 5);
            _forwardRegionMap.put("Atlantic", 6);
            _forwardRegionMap.put("Indian", 7);
            _forwardRegionMap.put("Other", 8);

        }

        private void loadIATAtoAirportMap(){
            BufferedReader br = null;
            String line;
            String cvsSplitBy = ",";
            try {
                br = new BufferedReader(new FileReader(IATA_LIST_PATH));
                while ((line = br.readLine()) != null) {
                    String[] row = line.split(cvsSplitBy);
                    String iataCode = row[IATA_COL].replace("\"", "");
                    String unparsedTimeZone = row[REGION_COL].replace("\"", "");

                    if (!iataCode.isEmpty() && !unparsedTimeZone.isEmpty()) {
                        String region = unparsedTimeZone.split("/")[0];
                        _iataToRegion.put(iataCode, region);
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }
}
