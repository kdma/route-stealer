package Dividers;

import RouteStealer.AirTypes.JRouteConstants;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by kdma on 01/05/2015.
 */
public class AllRoutesDivider extends RegionDivider {
    private final static Logger LOGGER = Logger.getLogger(AllRoutesDivider.class.getName());
    private final String W_ALL_ROUTES_PATH = "c:\\regions_output\\";

    public AllRoutesDivider(JSONArray routes){
       super(routes);
    }

    public void DivideByRegion(){
        for(int i=0;i<_routes.length(); i++){
            JSONObject route = (JSONObject) _routes.get(i);
            String departure = route.getString(JRouteConstants.DEPARTURE);
            String arrival = route.getString(JRouteConstants.ARRIVAL);
            String depRegion = _map.getRegionFromIata(departure);
            String arrRegion = _map.getRegionFromIata(arrival);
            moveRouteIntoCorrectRegion(route, depRegion, arrRegion);
            LOGGER.log(Level.INFO, "get region for {0}", departure);
        }
        WriteByRegion();
        WriteAll();

    }

    protected void WriteByRegion() {
        for (int i = 0; i < REGIONS_NUMBER; i++) {
            try {
                FileWriter file = new FileWriter(W_ALL_ROUTES_PATH+_regions[i].getRegionDenomination()+".json");
                file.write(_regions[i].GetRoutes().toString());
                file.flush();
                file.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void WriteAll() {
        try {
            FileWriter file = new FileWriter(W_ALL_ROUTES_PATH + "global.json");
            file.write(_routes.toString());
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
