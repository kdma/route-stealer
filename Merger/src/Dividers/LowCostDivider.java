package Dividers;

import org.json.JSONArray;
import java.io.FileWriter;

/**
 * Created by kdma on 05/05/2015.
 */
public class LowCostDivider extends RegionDivider {

    private final String W_LOW_COST_PATH = "c:\\regions_output\\lowcost.json";

    public LowCostDivider(JSONArray routes){
        super(routes);
    }

    @Override
    protected void WriteByRegion() {
            try {
                FileWriter file = new FileWriter(W_LOW_COST_PATH);
                file.write(getEuropeRoutes().toString());
                file.flush();
                file.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

