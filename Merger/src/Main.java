import Dividers.AllRoutesDivider;
import Dividers.LowCostDivider;

/**
 * Created by kdma on 18/04/2015.
 */
public class Main {
    public static void main(String[] args) {


        Merger merger = new Merger();
        merger.MergeAll();
        //merger.MergeLowCost();
        //merger.writeListSortedByWeight();
        //AllRoutesDivider allRoutesDivider = new AllRoutesDivider(merger.getAllRoutes());
        //LowCostDivider lowCostDivider = new LowCostDivider(merger.getLowCostRoutes());
        //System.out.println("dividing by region low cost");
        //lowCostDivider.DivideByRegion();
        //allRoutesDivider.DivideByRegion();
        //System.out.println("dividing by region all routes");
        Stats stat = new Stats(merger);
        RoutesWithInfoWriter _coordAdder = new RoutesWithInfoWriter();
        _coordAdder.WriteRouteWithAdditionalInfo();
        System.out.print("Unique airports: " + stat.getNumberOfUniqueAirports() + "\n");
        System.out.print("Routes : " + stat.getNumberOfRoutes()+ "\n");
        System.out.print("Graph density : " + stat.getGraphDensity()+ "\n");
        //TODO rendi path relativi e crea directory se non presenti
    }
}
